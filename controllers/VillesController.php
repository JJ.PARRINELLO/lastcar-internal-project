<?php

/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 29/05/18
 * Time: 23:53
 */

namespace Lastcar\controllers;

use Lastcar\core\Controller;
use Lastcar\core\Response;
use Lastcar\core\Utilities;
use Lastcar\models\Villes;

class VillesController extends Controller {

    use Utilities;


    public function getById() {
        $aUserId = $this->getParamFromUri();
        $id = $aUserId["getbyid"];
//        $id = explode("/", $this->get);
//        $id = (int)end($id);
        $modelUser = new Users();
        $modelUser->setId($id);
        $user = $modelUser->Retrieve();
        $aUser["user"] = $user;
        new Response($this->setUriToViewFormat(), $aUser);
//        vardump($oUser);
//        vardump((array)$oUser);
    }

    public function delete() {
        $id = explode("/", $_SERVER["REQUEST_URI"]);
        $id = (int) end($id);
        $modelUser = new Users();
        $modelUser->setId($id);
        $oUser = $modelUser->remove();
//        vardump($oUser);
    }

    public function getAllLike(){
        vardump($this->inputPost());
        new Response("/show/list/trips");
    }

    public function autoCompletionInput() {

        try {
            vardump($this->inputGet());
            $modelVille = new Villes();
            $datas['users'] = $modelVille->getAllLike();

        } catch (\Exception $e) {
            //new RenderMessage($e);
            exit;
        }
    }

}