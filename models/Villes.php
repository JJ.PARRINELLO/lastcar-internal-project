<?php
/**
 * Created by EntitiesBuilder.
 * User: padbrain
 * Date: 04/07/2018
 */

namespace Lastcar\models;


class Villes extends EntityModel
{
     private $Code_commune_INSEE;
     private $Nom_commune;
     private $Code_postal;
     private $Libelle_acheminement;
     private $Ligne_5;
     private $Latt;
     private $Longe;


    /*
     *      GETTERS
     */


                        /**
                         * @return string
                         */
    
                        public function getCode_commune_INSEE()
                        {
                            return $this->Code_commune_INSEE;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getNom_commune()
                        {
                            return $this->Nom_commune;
                        }

            
                        /**
                         * @return int
                         */
    
                        public function getCode_postal()
                        {
                            return $this->Code_postal;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLibelle_acheminement()
                        {
                            return $this->Libelle_acheminement;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLigne_5()
                        {
                            return $this->Ligne_5;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLatt()
                        {
                            return $this->Latt;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLonge()
                        {
                            return $this->Longe;
                        }

            

    /*
     *      SETTERS
     */


                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setCode_commune_INSEE($Code_commune_INSEE)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Code_commune_INSEE)){
                                $this->Code_commune_INSEE = $Code_commune_INSEE;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setNom_commune($Nom_commune)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Nom_commune)){
                                $this->Nom_commune = $Nom_commune;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param int
                         * @return bool
                         */
    
                        public function setCode_postal($Code_postal)
                        {
                           if(preg_match("#^[0-9]+#", $Code_postal)){
                                $this->Code_postal = $Code_postal;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLibelle_acheminement($Libelle_acheminement)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Libelle_acheminement)){
                                $this->Libelle_acheminement = $Libelle_acheminement;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLigne_5($Ligne_5)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Ligne_5)){
                                $this->Ligne_5 = $Ligne_5;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLatt($Latt)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Latt)){
                                $this->Latt = $Latt;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLonge($Longe)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Longe)){
                                $this->Longe = $Longe;
                                return true;
                           }else{
                                return false;
                           }
                        }

            



    /*
     *      CRUD TO SEND CUSTOMIZED EXCEPTIONS FROM FAILED QUERIES
     */
    public function Create($post) {
        $response = parent::Create($post);
        if($response == 0 ):
            throw new \Exception("La création de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Retrieve(){
        $response = parent::Retrieve();
        if($response == 0 ):
            throw new \Exception("La récupération de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Update($aPropVal){
        $response = parent::Update($aPropVal);
        if($response == 0 ):
            throw new \Exception("La mise à jour de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Delete(){
        $response = parent::Delete();
        if($response == 0 ):
            throw new \Exception("La suppression de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }
}<?php
/**
 * Created by EntitiesBuilder.
 * User: padbrain
 * Date: 05/07/2018
 */

namespace Lastcar\models;


class Villes extends EntityModel
{
     private $Code_commune_INSEE;
     private $Nom_commune;
     private $Code_postal;
     private $Libelle_acheminement;
     private $Ligne_5;
     private $Latt;
     private $Longe;


    /*
     *      GETTERS
     */


                        /**
                         * @return string
                         */
    
                        public function getCode_commune_INSEE()
                        {
                            return $this->Code_commune_INSEE;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getNom_commune()
                        {
                            return $this->Nom_commune;
                        }

            
                        /**
                         * @return int
                         */
    
                        public function getCode_postal()
                        {
                            return $this->Code_postal;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLibelle_acheminement()
                        {
                            return $this->Libelle_acheminement;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLigne_5()
                        {
                            return $this->Ligne_5;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLatt()
                        {
                            return $this->Latt;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLonge()
                        {
                            return $this->Longe;
                        }

            

    /*
     *      SETTERS
     */


                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setCode_commune_INSEE($Code_commune_INSEE)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Code_commune_INSEE)){
                                $this->Code_commune_INSEE = $Code_commune_INSEE;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setNom_commune($Nom_commune)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Nom_commune)){
                                $this->Nom_commune = $Nom_commune;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param int
                         * @return bool
                         */
    
                        public function setCode_postal($Code_postal)
                        {
                           if(preg_match("#^[0-9]+#", $Code_postal)){
                                $this->Code_postal = $Code_postal;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLibelle_acheminement($Libelle_acheminement)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Libelle_acheminement)){
                                $this->Libelle_acheminement = $Libelle_acheminement;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLigne_5($Ligne_5)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Ligne_5)){
                                $this->Ligne_5 = $Ligne_5;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLatt($Latt)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Latt)){
                                $this->Latt = $Latt;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLonge($Longe)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Longe)){
                                $this->Longe = $Longe;
                                return true;
                           }else{
                                return false;
                           }
                        }

            



    /*
     *      CRUD TO SEND CUSTOMIZED EXCEPTIONS FROM FAILED QUERIES
     */
    public function Create($post) {
        $response = parent::Create($post);
        if($response == 0 ):
            throw new \Exception("La création de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Retrieve(){
        $response = parent::Retrieve();
        if($response == 0 ):
            throw new \Exception("La récupération de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Update($aPropVal){
        $response = parent::Update($aPropVal);
        if($response == 0 ):
            throw new \Exception("La mise à jour de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Delete(){
        $response = parent::Delete();
        if($response == 0 ):
            throw new \Exception("La suppression de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }
}<?php
/**
 * Created by EntitiesBuilder.
 * User: padbrain
 * Date: 05/07/2018
 */

namespace Lastcar\models;


class Villes extends EntityModel
{
     private $Code_commune_INSEE;
     private $Nom_commune;
     private $Code_postal;
     private $Libelle_acheminement;
     private $Ligne_5;
     private $Latt;
     private $Longe;


    /*
     *      GETTERS
     */


                        /**
                         * @return string
                         */
    
                        public function getCode_commune_INSEE()
                        {
                            return $this->Code_commune_INSEE;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getNom_commune()
                        {
                            return $this->Nom_commune;
                        }

            
                        /**
                         * @return int
                         */
    
                        public function getCode_postal()
                        {
                            return $this->Code_postal;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLibelle_acheminement()
                        {
                            return $this->Libelle_acheminement;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLigne_5()
                        {
                            return $this->Ligne_5;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLatt()
                        {
                            return $this->Latt;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLonge()
                        {
                            return $this->Longe;
                        }

            

    /*
     *      SETTERS
     */


                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setCode_commune_INSEE($Code_commune_INSEE)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Code_commune_INSEE)){
                                $this->Code_commune_INSEE = $Code_commune_INSEE;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setNom_commune($Nom_commune)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Nom_commune)){
                                $this->Nom_commune = $Nom_commune;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param int
                         * @return bool
                         */
    
                        public function setCode_postal($Code_postal)
                        {
                           if(preg_match("#^[0-9]+#", $Code_postal)){
                                $this->Code_postal = $Code_postal;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLibelle_acheminement($Libelle_acheminement)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Libelle_acheminement)){
                                $this->Libelle_acheminement = $Libelle_acheminement;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLigne_5($Ligne_5)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Ligne_5)){
                                $this->Ligne_5 = $Ligne_5;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLatt($Latt)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Latt)){
                                $this->Latt = $Latt;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLonge($Longe)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Longe)){
                                $this->Longe = $Longe;
                                return true;
                           }else{
                                return false;
                           }
                        }

            



    /*
     *      CRUD TO SEND CUSTOMIZED EXCEPTIONS FROM FAILED QUERIES
     */
    public function Create($post) {
        $response = parent::Create($post);
        if($response == 0 ):
            throw new \Exception("La création de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Retrieve(){
        $response = parent::Retrieve();
        if($response == 0 ):
            throw new \Exception("La récupération de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Update($aPropVal){
        $response = parent::Update($aPropVal);
        if($response == 0 ):
            throw new \Exception("La mise à jour de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Delete(){
        $response = parent::Delete();
        if($response == 0 ):
            throw new \Exception("La suppression de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }
}<?php
/**
 * Created by EntitiesBuilder.
 * User: padbrain
 * Date: 05/07/2018
 */

namespace Lastcar\models;


class Villes extends EntityModel
{
     private $Code_commune_INSEE;
     private $Nom_commune;
     private $Code_postal;
     private $Libelle_acheminement;
     private $Ligne_5;
     private $Latt;
     private $Longe;


    /*
     *      GETTERS
     */


                        /**
                         * @return string
                         */
    
                        public function getCode_commune_INSEE()
                        {
                            return $this->Code_commune_INSEE;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getNom_commune()
                        {
                            return $this->Nom_commune;
                        }

            
                        /**
                         * @return int
                         */
    
                        public function getCode_postal()
                        {
                            return $this->Code_postal;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLibelle_acheminement()
                        {
                            return $this->Libelle_acheminement;
                        }

            
                        /**
                         * @return string
                         */
    
                        public function getLigne_5()
                        {
                            return $this->Ligne_5;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLatt()
                        {
                            return $this->Latt;
                        }

            
                        /**
                         * @return double
                         */
    
                        public function getLonge()
                        {
                            return $this->Longe;
                        }

            

    /*
     *      SETTERS
     */


                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setCode_commune_INSEE($Code_commune_INSEE)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Code_commune_INSEE)){
                                $this->Code_commune_INSEE = $Code_commune_INSEE;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setNom_commune($Nom_commune)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Nom_commune)){
                                $this->Nom_commune = $Nom_commune;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param int
                         * @return bool
                         */
    
                        public function setCode_postal($Code_postal)
                        {
                           if(preg_match("#^[0-9]+#", $Code_postal)){
                                $this->Code_postal = $Code_postal;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLibelle_acheminement($Libelle_acheminement)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Libelle_acheminement)){
                                $this->Libelle_acheminement = $Libelle_acheminement;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param string
                         * @return bool
                         */
    
                        public function setLigne_5($Ligne_5)
                        {
                           if(preg_match("#^[a-zA-Z(0-9)?(/%-\*\+$\.)?]{1,}$#", $Ligne_5)){
                                $this->Ligne_5 = $Ligne_5;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLatt($Latt)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Latt)){
                                $this->Latt = $Latt;
                                return true;
                           }else{
                                return false;
                           }
                        }

            
                        /**
                         * @param double
                         * @return bool
                         */
    
                        public function setLonge($Longe)
                        {
                           if(preg_match("#^[0-9]{1,}\.[0-9]{1,}$#", $Longe)){
                                $this->Longe = $Longe;
                                return true;
                           }else{
                                return false;
                           }
                        }

            



    /*
     *      CRUD TO SEND CUSTOMIZED EXCEPTIONS FROM FAILED QUERIES
     */
    public function Create($post) {
        $response = parent::Create($post);
        if($response == 0 ):
            throw new \Exception("La création de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Retrieve(){
        $response = parent::Retrieve();
        if($response == 0 ):
            throw new \Exception("La récupération de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Update($aPropVal){
        $response = parent::Update($aPropVal);
        if($response == 0 ):
            throw new \Exception("La mise à jour de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }

    public function Delete(){
        $response = parent::Delete();
        if($response == 0 ):
            throw new \Exception("La suppression de 'Villes' n'a pas pu être effectuée !");
        else:
            return $response;
        endif;
    }
}